package com.demo;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Demoshop {
	
	
	public WebDriver driver;
	@BeforeClass
	public void lauchbrowser()
	{
		driver =new ChromeDriver();
		
		driver.get("https://demowebshop.tricentis.com/login/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);	
	}
	
	@Test
	public void a() {
			driver.findElement(By.id("Email")).sendKeys("Demo@gmail.com");
		driver.findElement(By.id("Password")).sendKeys("Demo@123");
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
	}
	
	@AfterClass
	public void closebrowser()
	{
		driver.close();
	}
	
	

}
