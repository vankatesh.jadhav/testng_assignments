package com.testng;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Grouping_TestCases {
	
	@Test (groups={"Smoke Test"})
	public void modifyCustomer() {
		System.out.println("The Customer Get Modified");
		
	}
	
	
	@Test (groups={"Regression Test"})
	public void createCustomer() {
		
		System.out.println("Customer created");
		
		
	}
	
	@Test (groups={"Usabilty Test"})
	public void newCustomer() {
		System.out.println("New Coustmer will Get Created");
		
	}
	
	@BeforeClass 
  public void brforeClass() {
	  System.out.println("Before class");
  }

	@AfterClass
	 public void afterClass() {
		  System.out.println("After class");
	  }

}
